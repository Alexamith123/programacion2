package ejerciciosprimeraclase;
/**
 *
 * @author Samir Portillo
 */
public class Metodos {
    
    
    private int salir = 0;
    
    Persona personita[][] = new Persona[5][5];

    public int getSalir() {
        return salir;
    }

    public void setSalir(int salir) {
        this.salir = salir;
    }
    public String suerte(int x,int y, int z){
        String res = "";
        int suma = x + y + z;
        int num1 = suma/1000;      
        int num2 = suma/100%10;  
        int num3 = suma/10%10;   
        int num4 = suma%10;        
        int total = num1 + num2 + num3 + num4;
        res = "La suma de la fecha es: " + total;
        return res;
    }
    
    public String perfecto(int x){
        String res = "";
        int contador = 0;
        for(int y = 1 ;y < x; y++){
            if(x % y  == 0){
                contador = contador + y;
                if(contador == x){
                    res = "El numero "+x+" es perfecto";
                }
                else{
                     res = "El numero "+x+" no es perfecto";
                }
            }
        }

        return res;
    }

    public String acertar(int numAcertar,int ram,int intentos) {
        System.out.println(ram + "\n" + intentos);
       String res = "";
       
       if(numAcertar == ram){
            res = "Felicidades acertó el numero";
            salir = 1;
        }
       else if(numAcertar > ram){
            res = "El numero es menor";
           
       }
       else if(numAcertar < ram){
            res = "El numero es mayor";
           
       }
       if(intentos == 1){
           res = "Perdió";
           salir = 1;
       }
     
       return res;
    }
    
    
    public String llenarMatriz(String nombre, int edad){
        String res = "";
        for (int f = 0; f < personita.length; f++) {
            for(int c = 0; c < personita[f].length; c++){
                personita[f][c] = new Persona(nombre, edad);
            res += personita[f][c] + ",";
            }
            res += "\b\b\n";
                }
        
        return res ;
    }
    
    public String imprimir() {
        String res = "";
        for (int f = 0; f < personita.length; f++) {
            for (int c = 0; c < personita[f].length; c++) {
                res += personita[f][c] + ", ";
            }
            res += "\b\b\n";
        }
        return res;
    }
    
    public String rima (String pala1, String pala2){
        String  res = "";
        int antePenultimaWord1 = pala1.length()-3;
        int penultimaWord1 = pala1.length()-1;
        int finalWordWord1 = pala1.length();
        String subWord1 = pala1.substring(antePenultimaWord1,finalWordWord1);
        String sub2Word1 = pala1.substring(penultimaWord1,finalWordWord1);
        int antePenultimaWord2 = pala2.length()-3;
        int penultimaWord2 = pala2.length()-1;
        int finalWordWord2 = pala2.length();
        String subWord2 = pala2.substring(antePenultimaWord2,finalWordWord2);
        String sub2Word2 = pala2.substring(penultimaWord2,finalWordWord2);
        if(subWord1.equals(subWord2)){
            res += "Las palabras riman\n";
        }
        else if(sub2Word1.equals(sub2Word2)){
             res += "Las palabras casi riman";
        }
        else{
            res += "Las palabras no riman";
        }
        return res;
    }

}


            
        