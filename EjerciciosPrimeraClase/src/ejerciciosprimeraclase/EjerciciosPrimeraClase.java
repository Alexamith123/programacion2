package ejerciciosprimeraclase;

import javax.swing.JOptionPane;
/**
 *
 * @author Samir Portillo
 */
public class EjerciciosPrimeraClase {

    public static void main(String[] args) {
        
        
        
       Metodos log = new Metodos();
       while(true){
       int menu = Integer.parseInt(JOptionPane.showInputDialog("Menú\n"
               + "1. Numero de la suerte\n"
               + "2. Numero perfecto\n"
               + "3. Acertar el numero\n"
               + "4. Insertar nombres y edades\n"
               + "5. Letra que rima\n"
               + "6. Salir"));
       if(menu == 6 || menu > 6 || menu < 1){
           break;
       }
       
       switch(menu){
           case 1:
                int dia = Integer.parseInt(JOptionPane.showInputDialog("Día de nacimiento:  "));
                int mes = Integer.parseInt(JOptionPane.showInputDialog("Mes de nacimiento:  "));
                int ano = Integer.parseInt(JOptionPane.showInputDialog("Año de nacimiento:  "));
                JOptionPane.showMessageDialog(null, log.suerte(dia, mes, ano), "MENSAJE", JOptionPane.PLAIN_MESSAGE);
                break;
           case 2:
               int numero = Integer.parseInt(JOptionPane.showInputDialog("Digite el numero para averiguar si es perfecto:   "));
               JOptionPane.showMessageDialog(null, log.perfecto(numero), "MENSAJE", JOptionPane.PLAIN_MESSAGE);
               break;
               
           case 3:
               int ram = (int)(Math.random()*15)+1;
               int i = 3;
               while(i >= 0){  
               if(log.getSalir() == 1){
                   log.setSalir(0);
                   break;
               }
               int numAcertar = Integer.parseInt(JOptionPane.showInputDialog("Digite el numero: "));
               JOptionPane.showMessageDialog(null,log.acertar(numAcertar, ram, i),"MENSAJE",JOptionPane.PLAIN_MESSAGE);
               i--;
               
               }
               break;
               
           case 4:
               while (true){
                String nombre = JOptionPane.showInputDialog("Digite su nombre:  ");
                int edad = Integer.parseInt( JOptionPane.showInputDialog("Digite su edad:  "));
                log.llenarMatriz(nombre, edad);
                Persona resolver = new Persona(nombre, edad);
//                JOptionPane.showMessageDialog(null,resolver.getNombre()+" "+resolver.getEdad(),"MENSAJE",JOptionPane.PLAIN_MESSAGE);
                JOptionPane.showMessageDialog(null,log.imprimir(),"MENSAJE",JOptionPane.PLAIN_MESSAGE);
                
                int ingresarOtroNombre = Integer.parseInt(JOptionPane.showInputDialog("Desea ingresar otro nombre: \n"
                        + "1. si\n"
                        + "2. no"));
                if(ingresarOtroNombre == 2){
                    break;
                }
               }
               
           case 5:
               while(true){
                String palabra1 = JOptionPane.showInputDialog("Ingrese la primera palabra: ");
                String palabra2 = JOptionPane.showInputDialog("Ingrese la segunda palabra: ");
                JOptionPane.showMessageDialog(null,log.rima(palabra1,palabra2),"MENSAJE",JOptionPane.PLAIN_MESSAGE);
                int ingresarOtraPalabra = Integer.parseInt(JOptionPane.showInputDialog("Desea ingresar otra palabra: \n"
                        + "1. si\n"
                        + "2. no"));
                 if(ingresarOtraPalabra == 2){
                    break;
                }
               }
               
       }
       
       }
       
      
       
       
       
       
       
      
    }
    
}
