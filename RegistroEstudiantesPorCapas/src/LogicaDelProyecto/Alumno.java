/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LogicaDelProyecto;

/**
 *
 * @author Samir Portillo
 */
public class Alumno {
    private String nom;
    private String apell;
    private int n1;
    private int n2;
    private int n3;

    public Alumno(String nom, String apell, int n1, int n2, int n3) {
        this.nom = nom;
        this.apell = apell;
        this.n1 = n1;
        this.n2 = n2;
        this.n3 = n3;
    }

   

    
     
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getApell() {
        return apell;
    }

    public void setApell(String apell) {
        this.apell = apell;
    }

    public int getN1() {
        return n1;
    }

    public void setN1(int n1) {
        this.n1 = n1;
    }

    public int getN2() {
        return n2;
    }

    public void setN2(int n2) {
        this.n2 = n2;
    }

    public int getN3() {
        return n3;
    }

    public void setN3(int n3) {
        this.n3 = n3;
    }
    
    
}
