package TodoSobreDatos;

import LogicaDelProyecto.Alumno;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author Samir Portillo
 */
public class RegistrarEstudiantesDatos {

    
    public void escribirEnELArchivo(Alumno alumno){
 
    String res = "";  
        try {
            File archivo = new File("registros\\RegistroAlumnos.txt");
            if(archivo.exists()) {
                FileWriter archi = new FileWriter(archivo,true);
                BufferedWriter bw = new BufferedWriter(archi);
                bw.write(alumno.getNom()+","+alumno.getApell()+","+alumno.getN1()+","+alumno.getN2()+","+alumno.getN3());
                bw.newLine();
                bw.close();
            }else{
                FileWriter archi = new FileWriter(archivo);
                BufferedWriter bw = new BufferedWriter(archi);
                bw.write(alumno.getNom()+","+alumno.getApell()+","+alumno.getN1()+","+alumno.getN2()+","+alumno.getN3());
                bw.newLine();
                bw.close();
            }


        } catch (IOException e) {
           res +="Error al escribir en el archivo y LEERLO" + e;
        }

    }   
    

     public String leerArchivoEstudiantes() {
        String inString = "";
        try {
            File archivo = new File("Administrador\\RegistroEstudiantes.txt");

            FileReader leer = new FileReader(archivo);
            BufferedReader inStream = new BufferedReader(leer);
            inString = inStream.readLine();
            inStream.close();

        } catch (IOException e) {
            System.out.println("no se ha podido leer el archivo" + e);
        }
        return inString;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
