package empresa;
/**
 *
 * @author Samir Portillo
 */
public class ProductosCongelados extends Producto{
    public int fechaEnvasado3;
    public String paisOrigen3;
    public int temperatura;

    public ProductosCongelados(int fechaEnvasado3, String paisOrigen3, int temperatura, int fechaCaducidad, int numeroLote) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasado3 = fechaEnvasado3;
        this.paisOrigen3 = paisOrigen3;
        this.temperatura = temperatura;
    }

  public String getAtributos() {

        return "Fecha de caducidad: " + fechaCaducidad
                + "\n Numero de lote: " + numeroLote
                + "\n Fecha envasado:" + fechaEnvasado3
                + "\n País de origen : " + paisOrigen3
                + "\n Temperatura: " + temperatura;
               
    }
    
    
}
