package empresa;
/**
 *
 * @author Samir Portillo
 */
public class Empresa {

    public static void main(String[] args) {
        Producto producto = new Producto(0, 0);
        ProductosFrescos productosFrescos1 = new ProductosFrescos(0, "Brazil", 0, 0);
        ProductosRefrigerados productosRefrigerados = new ProductosRefrigerados(0, 0, 0, "España", 0, 0);
        ProductosCongelados productosCongelados = new ProductosCongelados(0, "Holanda", 0, 0, 0);
        CongeladosAire congeladosAire = new CongeladosAire(0, 0, 0, 0, 0, "Afganistán", 0, 0, 0);
        CongeladosAgua congeladosAgua = new CongeladosAgua(0, 0, "Alemania", 0, 0, 0);
        CongeladosNitrogeno congeladosNitrogeno = new CongeladosNitrogeno(0, 0, "Francia", 0, 0, 0);
        
        System.out.println("Productos frescos: "+"\n"+ productosFrescos1.getAtributos()+"\n"+
                "\n Productos refrigerados: "+"\n"+productosRefrigerados.getAtributos()+"\n"+
                "\n Productos congelados: " +"\n"+productosCongelados.getAtributos()+"\n"+
                "\n Productos congelados por aire: " +"\n"+congeladosAire.getAtributos()+"\n"+
                "\n Productos congelados por agua: " +"\n"+congeladosAgua.getAtributos()+"\n"+
                "\n Productos congelados en nitrogeno: " +"\n"+congeladosNitrogeno.getAtributos());
    }
    
}
