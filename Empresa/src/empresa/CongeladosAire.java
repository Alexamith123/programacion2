package empresa;
/**
 *
 * @author Samir Portillo
 */
public class CongeladosAire extends ProductosCongelados{
    public int nitrogeno;
    public int oxigeno;
    public int dioxidoCarbono;
    public int vaporAgua;

    public CongeladosAire(int nitrogeno, int oxigeno, int dioxidoCarbono, int vaporAgua, int fechaEnvasado3, String paisOrigen3, int temperatura, int fechaCaducidad, int numeroLote) {
        super(fechaEnvasado3, paisOrigen3, temperatura, fechaCaducidad, numeroLote);
        this.nitrogeno = nitrogeno;
        this.oxigeno = oxigeno;
        this.dioxidoCarbono = dioxidoCarbono;
        this.vaporAgua = vaporAgua;
    }
    
      public String getAtributos() {

        return "Fecha de caducidad: " + fechaCaducidad
                + "\n Numero de lote: " + numeroLote
                + "\n Fecha envasado:" + fechaEnvasado3
                + "\n País de origen : " + paisOrigen3
                + "\n Temperatura: " + temperatura
                + "\n Cantidad de nitrogeno: " + nitrogeno
                + "\n Cantidad de óxigeno: " + oxigeno
                + "\n Cantidad de dióxido de carbono: " + dioxidoCarbono
                + "\n Cantidad de vapor del agua: " + vaporAgua;
            
               
    }
}
