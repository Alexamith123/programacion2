package empresa;
/**
 *
 * @author Samir Portillo
 */
public class Producto {
    public  int fechaCaducidad;
    public  int numeroLote;

    public Producto(int fechaCaducidad, int numeroLote) {
        this.fechaCaducidad = fechaCaducidad;
        this.numeroLote = numeroLote;
    }
 
    
     public String getAtributos() {

        return "fecha de caducidad: " + fechaCaducidad
                + "\n numeroLote: " + numeroLote;
                
    }

    
    
    
}
