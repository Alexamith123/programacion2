package empresa;
/**
 *
 * @author Samir Portillo
 */
public class CongeladosNitrogeno extends ProductosCongelados{
    public int segundosExpuestosAlNitrogeno;

    public CongeladosNitrogeno(int segundosExpuestosAlNitrogeno, int fechaEnvasado3, String paisOrigen3, int temperatura, int fechaCaducidad, int numeroLote) {
        super(fechaEnvasado3, paisOrigen3, temperatura, fechaCaducidad, numeroLote);
        this.segundosExpuestosAlNitrogeno = segundosExpuestosAlNitrogeno;
    }
    
      public String getAtributos() {

        return "Fecha de caducidad: " + fechaCaducidad
                + "\n Numero de lote: " + numeroLote
                + "\n Fecha envasado:" + fechaEnvasado3
                + "\n País de origen : " + paisOrigen3
                + "\n Temperatura: " + temperatura
                + "\n Tiempo expuesto al nitrógeno: " + segundosExpuestosAlNitrogeno + " segundos";
               
    }
}

