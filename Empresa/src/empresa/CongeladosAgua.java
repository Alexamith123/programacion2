package empresa;
/**
 *
 * @author Samir Portillo
 */
public class CongeladosAgua extends ProductosCongelados{
    public int salinidadAgua;

    public CongeladosAgua(int salinidadAgua, int fechaEnvasado3, String paisOrigen3, int temperatura, int fechaCaducidad, int numeroLote) {
        super(fechaEnvasado3, paisOrigen3, temperatura, fechaCaducidad, numeroLote);
        this.salinidadAgua = salinidadAgua;
    }

    
      public String getAtributos() {

        return "Fecha de caducidad: " + fechaCaducidad
                + "\n Numero de lote: " + numeroLote
                + "\n Fecha envasado:" + fechaEnvasado3
                + "\n País de origen : " + paisOrigen3
                + "\n Temperatura: " + temperatura
                + "\n Cantidad de salinidad: " + salinidadAgua;
               
    }
    
    
    
}
