package empresa;
/**
 *
 * @author Samir Portillo
 */
public class ProductosFrescos extends Producto{
    public int fechaEnvasado1;
    public String paisOrigen1;

    public ProductosFrescos(int fechaEnvasado1, String paisOrigen1, int fechaCaducidad, int numeroLote) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasado1 = fechaEnvasado1;
        this.paisOrigen1 = paisOrigen1;
    }
   
     public String getAtributos() {

        return "Fecha de caducidad: " + fechaCaducidad
                + "\n Numero de lote: " + numeroLote
                + "\n Fecha envasado:" + fechaEnvasado1
                + "\nPaís de origen: " + paisOrigen1;
               
    }


    
    
    
    
}
