package empresa;
/**
 *
 * @author Samir Portillo
 */
public class ProductosRefrigerados extends Producto{
    public int codigo;
    public int fechaEnvasado2;
    public int temperaturaMantenimiento;
    public String paisOrigen;

    public ProductosRefrigerados(int codigo, int fechaEnvasado2, int temperaturaMantenimiento, String paisOrigen, int fechaCaducidad, int numeroLote) {
        super(fechaCaducidad, numeroLote);
        this.codigo = codigo;
        this.fechaEnvasado2 = fechaEnvasado2;
        this.temperaturaMantenimiento = temperaturaMantenimiento;
        this.paisOrigen = paisOrigen;
    }

   public String getAtributos() {

        return   " Fecha de caducidad: " + fechaCaducidad
                + "\n Numero de lote: " + numeroLote
                +"\n Código: " + codigo
                + "\n Fecha envasado:" + fechaEnvasado2
                + "\n Temperatura: " + temperaturaMantenimiento
                + "\n País de origen : " + paisOrigen;
               
    }
    
    
}
