package herenciasegundaclase;
/**
 *
 * @author Samir Portillo
 */
public class Entrenador extends Seleccion{
    public int idFederacion;

    public Entrenador(int id, String nombre, String apellidos, int edad, int idFederacion) {
        super(id, nombre, apellidos, edad);
        this.idFederacion = idFederacion;
    }
     public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nPellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nId federacion: " + idFederacion;
    }
      
      public boolean concentrarse(){
          return true;
      }
      
      public boolean viajar(){
        return true;
    }
    
}
