package herenciasegundaclase;
/**
 *
 * @author Samir Portillo
 */
public class HerenciaSegundaClase {

    public static void main(String[] args) {
       
        Seleccion player = new Seleccion(0, "Roberto", "Gonzales", 0);
        
        Jugador n1 = new Jugador(0, "Luis", "Herrera", 0, 0, "Delantero");
        Entrenador coach = new Entrenador(0, "Santiago", "Pérez", 0, 0);
        Masajista mas = new Masajista("Master", 0, 0, "Daniel", "Herdandéz", 0);
        
        System.out.println(player.getAtributos());
        System.out.println("");
        
        System.out.println(n1.getAtributos());
        tipo(n1);
        System.out.println("");
        
        System.out.println(coach.getAtributos());
        tipo(coach);
        System.out.println("");
        
        System.out.println(mas.getAtributos());
        tipo(mas);
    }
    
     //polimorfismo
    public static void tipo (Seleccion seleccion){
        seleccion.concentrarse();
        seleccion.viajar();
    }
}
