package herenciasegundaclase;
/**
 *
 * @author Samir Portillo
 */
public class Masajista extends Seleccion{
    public String titulacio;
    public int experiencia;

    public Masajista(String titulacio, int experiencia, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        this.titulacio = titulacio;
        this.experiencia = experiencia;
    }
    
     public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nPellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nTitulación: " + titulacio
                + "\nAños de experiencia: " + experiencia;
    }
      
      public boolean concentrarse(){
          return false;
      }
      
      public boolean viajar(){
        return true;
    }
    
    
}
