/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herenciasegundaclase;

/**
 *
 * @author Samir Portillo
 */
public class Jugador extends Seleccion{
    public int dorsal;
    public String posicion;

    public Jugador(int id, String nombre, String apellidos, int edad, int dorsal, String posicion) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.posicion = posicion;
    }
      public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nPellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nDorsal: " + dorsal
                + "\nPosicion: " + posicion;
    }
      
      public boolean concentrarse(){
          return true;
      }
      
      public boolean viajar(){
        return true;
    }
    
}
