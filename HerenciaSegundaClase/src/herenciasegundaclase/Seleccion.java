
package herenciasegundaclase;
/**
 *
 * @author Samir Portillo
 */
public class Seleccion {
    public int id;
    public String nombre;
    public String apellidos;
    public int edad;

    public Seleccion(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }
       
    public String getAtributos() {

        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nPellidos:" + apellidos
                + "\nEdad: " + edad;
                
    }

    public boolean concentrarse(){
          return true;
      }
    
     public boolean viajar(){
        return true;
    }
}
